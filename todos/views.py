from multiprocessing import context
from django.urls import reverse_lazy
# from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView
from django.shortcuts import render
from todos.models import TodoItem, TodoList
# Create your views here.

class TodoListListView(ListView):
    model = TodoList
    template_name = "todo/list.html"


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todo/detail.html"


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todo/create.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("todo_detail", args=[self.object.id])

class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todo/delete.html"
    success_url = reverse_lazy("todo_list")


class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todo/edit.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("todo_detail", args=[self.object.id])




class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todoitems/create.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("todo_detail", args=[self.object.list.id])


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todoitems/edit.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("todo_detail", args=[self.object.list.id])