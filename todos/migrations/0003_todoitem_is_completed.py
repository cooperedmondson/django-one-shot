# Generated by Django 4.0.3 on 2022-05-03 18:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('todos', '0002_todoitem'),
    ]

    operations = [
        migrations.AddField(
            model_name='todoitem',
            name='is_completed',
            field=models.BooleanField(null=True),
        ),
    ]
